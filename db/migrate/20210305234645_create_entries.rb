class CreateEntries < ActiveRecord::Migration[6.0]
  def change
    create_table :entries do |t|
      t.integer :class
      t.float :maximum_weight
      t.string :movie
      t.references :competition, foreign_key: true
      t.references :user, foreign_key: true
      t.integer :time_zone
      t.timestamps
    end
  end
end
