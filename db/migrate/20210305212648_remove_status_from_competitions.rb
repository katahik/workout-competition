class RemoveStatusFromCompetitions < ActiveRecord::Migration[6.0]
  def change
    remove_column :competitions, :status, :integer
  end
end
