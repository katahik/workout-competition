class CreateCompetitions < ActiveRecord::Migration[6.0]
  def change
    create_table :competitions do |t|
      t.string :title
      t.string :type
      t.date :period_start
      t.date :period_end

      t.timestamps
    end
  end
end
