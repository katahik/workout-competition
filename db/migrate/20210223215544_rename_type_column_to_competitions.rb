class RenameTypeColumnToCompetitions < ActiveRecord::Migration[6.0]
  def change
    rename_column :competitions,:type,:competition_event
  end
end
