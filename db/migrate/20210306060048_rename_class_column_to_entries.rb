class RenameClassColumnToEntries < ActiveRecord::Migration[6.0]
  def change
    rename_column :entries, :class, :entry_class
    rename_column :entries, :movie, :video
    rename_column :entries, :maximum_weight, :lifted_weight
  end
end
