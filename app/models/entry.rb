class Entry < ApplicationRecord
    # carrierwaveの設定
    # アップロードファイル名が格納されるDBのカラム名とアップロードクラスを紐付ける
    mount_uploader :video, VideoUploader

    belongs_to :competition
    belongs_to :user
end
