class Competition < ApplicationRecord
    # オブジェクトがDBに保存される前のバリデーション
    # presence:出席 → 空でないこと
    validates :title, :competition_event, :period_start, :period_end, presence: true

    validate :start_end_check

    # 大会期間の日付の前後を検証
    def start_end_check
        errors.add(:period_end, "の日付を正しく記入してください。") if
            # period_end,startに値が入っていて、なおかつ、startがend以前である場合
            period_start && period_end && period_start > period_end
    end

    has_many :entries

end
