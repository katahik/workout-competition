class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable

  # database_authenticatable → DBに保存されたパスが正しいか検証。パスの暗号化も行う
  # registerable → 登録処理を通してサインアップ機能
  # recoverable → パスワードをリセットする
  # rememberable → 保存されたcookieからユーザーを記憶するトークンを作成
  # validatable → Emailやパスワードのバリデーションを追加する
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable
end
