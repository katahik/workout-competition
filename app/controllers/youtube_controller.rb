class YoutubeController < ApplicationController
    require 'google/apis/youtube_v3'
    GOOGLE_API_KEY = Rails.application.credentials.google[:api_key]

    def find_videos(keyword, after: 1.months.ago, before: Time.now)
        # 今回はGoogleAPIのYouTubeV3のYouTubeServiceのクラスを新規作成して、serviceに格納する
        # このYouTubeServiceは動画のアップロード、プレイリストの作成と管理、コンテンツの検索など、YouTubeのコア機能をサポート
        service = Google::Apis::YoutubeV3::YouTubeService.new
        service.key = GOOGLE_API_KEY

        next_page_token = nil

        options = {
            # キーワード検索を行うためqをキーにする.動画を検索したいためtypeをvideoにしている
            q: keyword,
            type: 'video',
            max_results: 3,
            order: :date,
            # 検索結果がmax_results以上である場合に、検索結果の中に入っている.検索した結果、次のページへ行くときに渡すトークン
            page_token: next_page_token,
            # published_にはRFC3339形式の日時の値を入れる必要があるため、iso8601メソッドで変換。
            published_after: after.iso8601,
            published_before: before.iso8601
        }

        # YouTubeServiceにはlist_〇〇メソッドが用意されている
        # list_〇〇〇(part名, デフォルトのnilから変更したいオプションキー)
        # 今回のpartパラメータは:snippetで、その値はopt
        # :snippetは複数オプションを指定したいときに使う
        service.list_searches(:snippet, options)
    end

    def index
        @youtube_data = find_videos('engineer')
    end
end