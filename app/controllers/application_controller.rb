class ApplicationController < ActionController::Base
    # CSRF対策
    # Railsで生成されるすべてのフォームとAjaxリクエストにセキュリティトークンが自動的に含まれる
    # htmlとcookiesのセキュリティトークンがマッチしない場合には例外がスローされる。
    protect_from_forgery with: :exception

    # devise_controller?とはdeviseを生成したときにできるヘルパーメソッド
    # if: :devise_controller でdeviseにまつわる画面に遷移したときに ということ
    # こうすることですべての画面で:configure_permitted_parametersするのを防ぐ
    before_action :configure_permitted_parameters, if: :devise_controller?

    protected

    # devise版のストロングパラメータ
    # added_attrsで保存できる項目を設定
    # そのadded_attrsを使ってdeviseの3つのアクションのキーに設定する
    def configure_permitted_parameters
        added_attrs = [ :email, :username, :password, :password_confirmation ]
        devise_parameter_sanitizer.permit :sign_up, keys: added_attrs
        devise_parameter_sanitizer.permit :account_update, keys: added_attrs
        devise_parameter_sanitizer.permit :sign_in, keys: added_attrs
    end
end
