class EntriesController < ApplicationController
    before_action :set_competition, only: %i[new]

    def new
        @entry = Entry.new
    end

    def create
        # paramsに入っているcompetition_idを取り出す
        @competition_id = Competition.find(params[:competition_id]).id

        @entry = Entry.new(entry_class: entry_params[:entry_class],
                           lifted_weight: entry_params[:lifted_weight],
                           video: entry_params[:video],
                           competition_id: @competition_id,
                           user_id: current_user.id)

        if @entry.save
            # bootstrapのflashを表示させる
            redirect_to "/competitions/#{@competition_id}", flash: {success: "登録が完了しました"}
        else
            flash.now[:danger] = '登録が失敗しました'
            render '/entries/new'
        end
    end

    # form_withでモデル指定でなく、url指定した場合はrequireオプションはいらない
    def entry_params
        params.permit(:entry_class, :lifted_weight, :video)
    end

    # paramsの中のcompetition_idに入っている値のみを取り出す
    def set_competition
        @competition = Competition.find(params[:competition_id])
    end

end
