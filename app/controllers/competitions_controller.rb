class CompetitionsController < ApplicationController
    # ヘルパーを読み込む
    include CompetitionsHelper
    # 各アクションを実行する前にset_competitionメソッドを実行
    # %i[]でシンボルの配列を作り出せる
    before_action :set_competition, only: %i[show edit update destroy]

    def index
        # ここでこの開催期間が今日以前であれば、 "開催前" 、含んでいれば "開催中" 、過ぎていれば "結果をみる"
        sorted_competitions =
            "SELECT *,
                    CASE
                        WHEN period_start > now() THEN 2
                        WHEN period_start <= now() && now() <= period_end THEN 1
                        WHEN period_end < now() THEN 3
                        ELSE 4
                    END AS status
            FROM competitions
            ORDER BY status ASC, period_end ASC;"

        @sorted_competitions = ActiveRecord::Base.connection.select_all(sorted_competitions).to_ary
    end

    def new
        # Competitionオブジェクトを作成して@competitionに格納
        @competition = Competition.new
    end

    def create
        # 今回はDBへの保存を一括で行うcreateを使うのではなく、
        # saveする際の挙動を場合分けしたいため　オブジェクトの生成(new)→保存(save)の二段階
        @competition = Competition.new(competition_params)

        if @competition.save
            # bootstrapのflashを表示させる
            redirect_to '/competitions', flash: {success: "登録が完了しました"}
        else
            flash.now[:danger] = '登録が失敗しました'
            render '/competitions/new'
        end
    end

    # competitionの詳細（ランキング画面）
    def show
        # competition_helperにカスタムヘルパーを設定
        light_entries = entries(3)
        @light_entries = ActiveRecord::Base.connection.select_all(light_entries).to_ary

        middle_entries = entries(6)
        @middle_entries = ActiveRecord::Base.connection.select_all(middle_entries).to_ary

        heavy_entries = entries(9)
        @heavy_entries = ActiveRecord::Base.connection.select_all(heavy_entries).to_ary
    end

    def edit
    end

    def update

        if @competition.update(competition_params)
            # bootstrapのflashを表示させる
            redirect_to @competition, flash: {success: "編集内容を保存しました"}
        else
            flash.now[:danger] = '登録が失敗しました'
            render 'edit'
        end
    end

    def destroy
        @competition.destroy
        redirect_to '/competitions', flash: {success: "削除が完了しました"}
    end


    private

    # strong parameters
    # 入力者から受け取る項目をcompeition_paramsに定義
    def competition_params
        params.require(:competition).permit(:title, :competition_event, :period_start, :period_end)
    end

    # require()の引数はform_withで指定したモデル名
    def set_competition
        @competition = Competition.find(params[:id])
    end

end
