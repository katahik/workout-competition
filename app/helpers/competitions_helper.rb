module CompetitionsHelper
    def entries(entry_class)
            "SELECT *,RANK() OVER (PARTITION BY entries.entry_class ORDER BY entries.lifted_weight DESC) AS rnk
                FROM (
                    SELECT entries.*,users.username
                        FROM entries
                            INNER JOIN users ON users.id = entries.user_id
                        WHERE entries.competition_id = #{@competition.id}
                            AND entries.entry_class = #{entry_class}
                ORDER BY entries.lifted_weight DESC, entries.updated_at
            ) AS t"
    end
end
