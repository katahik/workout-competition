Rails.application.routes.draw do
    devise_for :users
    root 'home#top'

    resources :users

    # ネストにすることで/competitions/1/entries/newとかができる
    resources :competitions do
        resources :entries, only: [:new, :create, :edit, :update, :destroy]
    end
get "/youtube" => "youtube#index"
end
